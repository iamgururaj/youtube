import React, {Component} from 'react';
import {VideoListItem} from 'components';

const VideoList = (props) => {
  const videoItems = props.videos.map((video) => {
    return <VideoListItem key= {video.etag} video={video}/>
  });

    return(
      <div >
        <ul>
          {videoItems}
        </ul>
      </div>
    );

};

export default VideoList;
