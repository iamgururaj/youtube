 import React from 'react';

 const VideoListItem = ({video}) => {
   // const video = props.video;
   const imageUrl = video.snippet.thumbnails.default.url;
   console.log(video)
   return(
     <li>
      <div>
        <img src={imageUrl} />
      </div>

      <div>
        <div> {video.snippet.title} </div>
      </div>

     </li>
   )
 }

export default VideoListItem;
