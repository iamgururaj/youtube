import React, {Component} from 'react';
import {SearchBar, VideoList} from 'components';
import YTSearch from 'youtube-api-search'

const API_KEY = "AIzaSyAZsDpvpRgrN4qkKB0z0pdfqCla2MvPqVk"

class HomePage extends Component {
  constructor(props){
    super(props);

    this.state = { videos: []}

    YTSearch({key: API_KEY,term: 'surfboards'}, (videos) => {
      this.setState({ videos });
    });
  }

  render(){
    return(
      <div>
        <SearchBar />
        <VideoList videos={this.state.videos}/>
      </div>
    )
  }
}

export default HomePage
